const express = require('express');
const router = express.Router();
const userRouter = require('./user.routes');
const authRouter = require('./auth.routes');


router.use(userRouter);
router.use(authRouter);

module.exports =router;