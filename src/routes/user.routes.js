const express = require('express');
const router = express.Router();
const user = require('../controllers/user.controller');
// const verifyToken = require('../helpers/verifyToken');

// create a new user
router.post('/users', user.create);
router.delete('/users/cleardb', user.deleteAll);
router.get('/users', /*verifyToken,*/ user.findAll);
router.get('/users/:id', /*verifyToken,*/ user.findById);
router.post('/users/:id/', user.findAndUpdate);
// router.put('/users/:id/', user.findAndUpdate);
router.delete('/users/:id/', user.deleteOne);


module.exports =router;