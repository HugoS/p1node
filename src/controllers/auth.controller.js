const User =require('../models/user.model');
const bcrypt = require ('bcrypt');
const jwt = require ('jsonwebtoken');
const spsecret = require('../configs/jwt.config')

exports.create = (req,res, err) => {
    let hashedPassword = bcrypt.hashSync(req.body.password,8);
    const user = new User({
        email : req.body.email,
        password : hashedPassword,
        firstname : req.body.firstname,
        lastname : req.body.lastname,
        admin : req.body.admin,
    })
    // if(err) {
    //     res.send(err);
    // }
    // else{
        user.save()
            .then(data=>{
                let token = jwt.sign(
                    {
                        id: user.email,
                        admin: user.admin
                    },
                    spsecret.secret,
                    {
                        expiresIn:86400
                    }
                )
                res.send({
                    auth:true,
                    token: token,                    
                    body:{
                        email:data.email,
                        firstname:data.firstname,
                    }
                });
            })
            .catch(err =>{
                res.status(500).send({
                    message: err.message
                })
            })
    // }

}

exports.login = (req, res) => {
    //requête pour retrouver un user en BDD-> findOne
    // bcrypt.compareSync(mot de passe envoyé, mot de passe ne base de données);

    // etape1: rechercher en base le user (avec email)
    User.findOne({email: req.body.email},
        function (err, user){
            if(!user) return res.status(404).send('No user found');
        // etape2: vérifier si mot de passe reçu = mot de passe en passe de base
            let mpdValide = bcrypt.compareSync(req.body.password, user.password);
        
            if(!mpdValide) return res.status(401).send({
                auth: false,
                token: null
            });
        // etape3: générer un nouveau token et on l'envoie dans la réponse
            else{
                let token = jwt.sign({
                    id: user._id,
                    admin: user.admin,
                    
                },
                spsecret.secret,
                {
                    expiresIn:86400
                }
            );
                res.status(200).send({
                    auth: true,
                    token: token,                    
                    data: user
                });
            }
            });
        }