const User = require('../models/user.model');
const bcrypt = require ('bcrypt');

exports.create = (req,res, err) => {
    let hashedPassword = bcrypt.hashSync(req.body.password,8);
    const user = new User({
        email : req.body.email,
        password : hashedPassword,
        firstname : req.body.firstname,
        lastname : req.body.lastname,
        admin : req.body.admin,
    })
    // if(err) {
    //     res.send(err);
    // }
    // else{
        user.save()
            .then(data=>{
                res.send(data)
            }).catch(err =>{
                res.status(500).send({
                    message: err.message
                })
            })
    // }
}
exports.findAll = (req, res) =>{
    User.find()
    .then(users =>{
        res.send(users);
    })
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occured when fiding users."
        })
    })
}
exports.findById = (req, res) =>{
    User.findById(req.params.id)
    .then(user=>{
        if (!user){
            return res.status(404).send({
                message: "User not found with id".req.params.id
            });
        }
        res.send(user)
    })
    
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occured when fiding user."
        })
    })
}
exports.findAndUpdate = (req, res)=>{
    console.log(req.body)
    User.findByIdAndUpdate(req.params.id, req.body)
    .then(user=>{
        if (!user){
            return res.status(404).send({
                message: "User not found"
            });
        }
        console.log(user.id);
        User.findById(req.params.id)
        .then(newUser=>{
            res.send(newUser);
        })
    })
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occured when fiding user."
        })
    })
}
exports.deleteOne=(req, res)=>{
    User.findOneAndRemove(req.params.id, req.body)
    .then(user=>{
        if(!user){
            return res.status(404).send({
                message: "User not found"
            })
        }
        res.send({
            message: `User with id ${req.params.id} deletes successfuly`
        })
    })
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occured when fiding user."
        })
    })
}
exports.deleteAll=(req, res)=>{
    User.deleteMany()
    .then(users=>{
        res.send(users)
    })
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occured when fiding user."
        })
    })
}